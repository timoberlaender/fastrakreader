module fastrakFeader{

	requires java.datatransfer;
	requires java.desktop;
	requires java.logging;
	requires vecmath;
	requires graph;
	requires math;
	requires rgg;
	requires imp3d;
	requires platform;
	requires platform.core;
	requires xl.core;
	requires utilities;

	
}
