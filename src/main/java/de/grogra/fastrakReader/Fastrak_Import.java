package de.grogra.fastrakReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.vecmath.Matrix3d;
import javax.vecmath.Point3d;
import javax.vecmath.Tuple3d;
import javax.vecmath.Vector3d;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import de.grogra.graph.impl.GraphManager;
import de.grogra.graph.impl.Node;
import de.grogra.pf.boot.Main;
import de.grogra.pf.registry.Item;
import de.grogra.util.Utils;
import de.grogra.vecmath.Math2;
import de.grogra.vecmath.geom.Line;

public class Fastrak_Import {
	Boolean diameterTax;
	public Fastrak_Import(InputStream inp, Node root) {
		XMLReader xmlReader = null;
		try {
			xmlReader = XMLReaderFactory.createXMLReader();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    FileReader reader = null;
//		try {
//			reader = new FileReader(f.getAbsolutePath());
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	    InputSource inputSource = new InputSource(inp);
	    Item ite = Item.resolveItem(Main.getRegistry(), "/fastrak/reader");
		String diameterKey = (String) Utils.get(ite, "diameter", "");
		String leafKey = (String) Utils.get(ite, "leafKey", "");

		diameterTax = (Boolean) Utils.get(ite, "diameterTaxonmy", "");
	    
	    XContentHandler xc=new XContentHandler(diameterKey,leafKey);
	    xmlReader.setContentHandler(xc);
	    try {
			xmlReader.parse(inputSource);
		} catch (IOException | SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    ArrayList<Branch> allBranches =xc.getAllBranches();
	    //checking for measurement artefacts: branches that are only one point which can't become Cylinders
	    for(int i =0; i< allBranches.size(); i++) {
	    	while(i< allBranches.size() && allBranches.get(i).points.size()<=1) {
	    		if(allBranches.get(i).parendId==-1) {
	    			allBranches.remove(i);
	    			allBranches.get(i).parendId=-1;
	    		}else {
	    			allBranches.remove(i);	    			
	    		}
	    	}
    	}
	    Branch treeRoot = null;
	    for(int i =0; i< allBranches.size(); i++) {
	    	Branch part = allBranches.get(i);
	     	if(part.parendId!=-1) {
	     		getParentBranchPoint(allBranches,i,part.points.get(0).getX(),part.points.get(0).getY(),part.points.get(0).getZ(),part.order);
	    	}else {
	    		treeRoot = part;
	    		
	    	}
	    }
	    
	    
//	    Node old = root;
	    
//	    for(Point p : treeRoot.points) {
//	    	System.err.println(p.childs);
//	    }

	    Node scaleton = new Node();
		scaleton.setName("branch");
		
	    Point p = treeRoot.points.get(0);
	    root.addEdgeBitsTo(scaleton, GraphManager.BRANCH_EDGE, null);
//	    Matrix3d rot= new Matrix3d();
//	    rot.setIdentity();
	    treeRoot.createGraph(root, allBranches, p.getX(), p.getY(),p.getZ(),null,null,scaleton);
	    
/*	    for(Branch part : allBranches) {
	    	if(part.parendId==-1) {
	    			part.createGraph(old,xc.getAllBranches(),0,0,0);
	    	}
	    }*/
	}
	
	    
	Branch getBranchByTag(String tag, ArrayList<Branch> list) {
		if(tag==null) {
			return list.get(0);
		}
		int i = tag.lastIndexOf('r');
		if (i==-1) {
			return list.get(0);
		}else {
			tag = tag.substring(0,i);
		}
		for(Branch b: list) {
			if(b.getTag().equals(tag)) {
				return b;
			}
		}
		return list.get(0);		
	}
	
	
	double getDistance(double startx, double starty, double startz, double d, double e, double f) {
		
		return Math.sqrt(Math.sqrt(Math.pow(startx-d,2)+Math.pow(starty-e,2))+Math.pow(startz-f,2));
	}
	
	double getDistanceSquared(double cSx, double cSy, double cSz, double pSx, double pSy, double pSz,double pEx, double pEy, double pEz , double pD) {
		Point3d childStart=new Point3d(cSx,cSy,cSz);
		Point3d parentStart=new Point3d(pSx,pSy,pSz);
		Point3d parentEnd=new Point3d(pEx,pEy,pEz);
		double len=parentStart.distance(parentEnd);
		Vector3d dir= new Vector3d(parentEnd);
		dir.sub(parentStart);
		Line parent = new Line(parentStart,dir,0,len);

		double u = Math2.dot (childStart, parentStart,dir)/dir.lengthSquared();
		double dist;
		if(u<0) {
			dist = parentStart.distance(childStart);//-(pD/2);
		}else if(u>len) {
			dist = parentEnd.distance(childStart);//-(pD/2);
		}else {
			dist = parent.distance(childStart);//-(pD/2);
		}
		return dist;
	}


	
	void getParentBranchPoint(ArrayList<Branch> posParent, int kiddoId, double startx,double starty,double startz, int order) {
		int minp=0;
		double tmp=0;
		int bid=0;
		double min=100000; 
		for(int pi=0; pi< posParent.size();pi++) {
			if(pi<kiddoId) {
			Branch parent=posParent.get(pi);
			if(parent.points.size()>1) {
				for(int i=0; i<parent.points.size()-1;i++) {
					tmp=getDistanceSquared(startx,starty,startz, parent.points.get(i).getX(),parent.points.get(i).getY(),parent.points.get(i).getZ(),parent.points.get(i+1).getX(),parent.points.get(i+1).getY(),parent.points.get(i+1).getZ(),parent.points.get(i).getDiameter());

					if(tmp<min &&(!diameterTax ||parent.points.get(i).getDiameter()>=posParent.get(kiddoId).points.get(0).getDiameter())) {
						minp=i;
						min=tmp;
						bid=pi;
					}
				}
			}
			}
		}
		Point x=posParent.get(bid).points.get(minp);
		x.addChild(kiddoId);
	}
}
