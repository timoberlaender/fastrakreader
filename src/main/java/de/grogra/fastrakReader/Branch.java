package de.grogra.fastrakReader;

import java.util.ArrayList;

import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import de.grogra.graph.Graph;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.Axis;
import de.grogra.imp3d.objects.Box;
import de.grogra.imp3d.objects.Cylinder;
import de.grogra.imp3d.objects.GlobalTransformation;
import de.grogra.imp3d.objects.Null;
import de.grogra.imp3d.objects.Parallelogram;
import de.grogra.math.TMatrix4d;
import de.grogra.turtle.AdjustLU;
import de.grogra.turtle.F;
import de.grogra.turtle.Translate;
import de.grogra.vecmath.Math2;
import de.grogra.vecmath.Matrix34d;

public class Branch {
	ArrayList<Point> points;
	String tag;
	int rootId;
	int parendId;
	int diameterId;
	int isLeafId;
	int order;

	boolean visited;

	public Branch(int pid) {
		visited = false;
		parendId = pid;
		points = new ArrayList<Point>();
		diameterId = 0;
	}

	public void addDiameter(float d) {
		points.get(diameterId).setDiameter(d/100);
		diameterId++;
	}

	public TMatrix4d setDirection(Vector3d direction) {
		final Matrix3d m = new Matrix3d();
		Math2.getOrthogonalBasis(direction, m, true);
		TMatrix4d tx = new TMatrix4d();
		tx.setRotationScale(m);
		return tx;
	}

	public void setRootId(int rootId) {
		this.rootId = rootId;
	}

	public void addPoint(float x, float y, float z) {
		points.add(new Point(x/100, y/100, z/100));
	}

	public Point getLastPoint() {
		int i = points.size();
		if (i == 0) {
			return null;
		}
		return points.get(i - 1);
	}

	public double getDistance(Point p, Point pn) {

		return Math.sqrt(Math.pow(p.getX() - pn.getX(), 2) + Math.pow(p.getY() - pn.getY(), 2)
				+ Math.pow(p.getZ() - pn.getZ(), 2));
	}

	public int getParendId() {
		return parendId;
	}

	public void createGraph(Node last, ArrayList<Branch> allRoots, double x,double y,double z, Matrix3d oldRot, Matrix4d gT, Node scaleton) {
		
		Matrix4d globalT;
		if(gT==null) {
			globalT= new Matrix4d();
			globalT.setIdentity();
		}else {
			globalT = new Matrix4d(gT);
			
		}

		Point p_old = points.get(0);
		Point p_new ;
		boolean isFirst=true;
		for(int i=1; i< points.size(); i++) {
			p_new= points.get(i);
			
			Vector3d a = new Vector3d(p_new.getX() - p_old.getX(), p_new.getY()  - p_old.getY() , p_new.getZ()  - p_old.getZ() );
			
			//get distance between points  -> length of F
			double len = a.length();
//			if(len>0) {
			//get rotation of F
			// 1. rotation between points
			Matrix3d globalRot = new Matrix3d();
			Math2.getOrthogonalBasis(a,globalRot, true);
			// 2. rotation of last point 
			Matrix3d localRot = new Matrix3d();
			if(oldRot==null) {
				localRot.setIdentity();
			}else {
				localRot.transpose(oldRot);
			}
			// 3. out cancel 
			localRot.mul(localRot,globalRot);			
			// set location to offSet

			TMatrix4d t = new TMatrix4d();
			t.m03 = x;
			t.m13 = y;
			t.m23 = z;
			t.setRotationScale(localRot);
			//update global transformation of the current turtle
			globalT.mul(t);
			F f = new F((float) len);
			f.setTransform(t);
			f.diameter=p_new.getDiameter();
			last.addEdgeBitsTo(f, (isFirst==true)?Graph.BRANCH_EDGE:Graph.SUCCESSOR_EDGE, null);
			scaleton.addEdgeBitsTo(f, Graph.REFINEMENT_EDGE, null);
			last = f;
			
			for(int rid : p_old.childs) {
				
				//get first point of child branch
				Point p= allRoots.get(rid).points.get(0);
				
				// get offset 
				Point3d offs = new Point3d(p.getX()- p_new.getX() , p.getY()-p_new.getY()   ,p.getZ() - p_new.getZ()  );
				
				//apply inverse transformation on the offset
				TMatrix4d t2 = new TMatrix4d(globalT);
				t2.transpose(globalT);
				Math2.transformPoint(t2, offs);

				Node scaletonKid = new Node();
				scaletonKid.setName("branch");
				scaleton.addEdgeBitsTo(scaletonKid, Graph.BRANCH_EDGE, null);
				allRoots.get(rid).createGraph(last, allRoots, offs.x,offs.y,offs.z, globalRot,globalT,scaletonKid);				
			}
			
			isFirst=false;
			oldRot = globalRot;
			x = 0;
			y = 0;
			z = 0;
			p_old=p_new;
//			}
		}
		
	}	
		
/*		Point p_old = points.get(0);

		Point p_new = points.get(1);
		
		F f = new F();
		f.diameter=p_new.getDiameter();
		setEndPoints(f,offSetx,offSety,offSetz,p_new.getX(),p_new.getY(),p_new.getZ());
		root.addEdgeBitsTo(f, Graph.BRANCH_EDGE, null);
		Node current= f;
//		p_new.x=p_new.x-offSetx;
//		p_new.y=p_new.y-offSetx;
//		p_new.z=p_new.z-offSetx;

		p_old = p_new;

		
		for(int i=2; i< points.size(); i++) {
			p_new = points.get(i);
			float desx=p_new.x-p_old.x;
			float desy=p_new.y-p_old.x;
			float desz=p_new.z-p_old.z;

			f = new F();
			f.diameter=p_new.getDiameter();

			setEndPoints(f,0.0,0.0,0.0,desx,desy,desz);
			current.addEdgeBitsTo(f, Graph.SUCCESSOR_EDGE, null);
			
			current=f;
			for(int rid : p_old.childs) {
				Point p= allRoots.get(rid).points.get(0);
				
				
				allRoots.get(rid).createGraph(current, allRoots,p.x-p_new.x, p.y-p_new.y, p.z-p_new.z);
			}
			
			p_old=p_new;
			
			
		}
		
		
		
		return root;
	}
*/
	public void setEndPoints(F f, double xs, double ys, double zs, double xe, double ye, double ze) {
		Vector3d a = new Vector3d(xe - xs, ye - ys, ze - zs);
		double len = a.length();
		f.length = (float) len;
		if (len > 0) {
			Matrix3d m = new Matrix3d();
			Math2.getOrthogonalBasis(a, m, true);
			TMatrix4d t = new TMatrix4d();
			t.m03 = xs;
			t.m13 = ys;
			t.m23 = zs;
			t.setRotationScale(m);
			f.setTransform(t);
		} else {
			f.setTransform(xs, ys, zs);
		}
	}

	/*
	 * public Node createGraph(Node root, ArrayList<Branch> allRoots, float
	 * offSetx,float offSety,float offSetz) { Point p = points.get(0); Translate t =
	 * new Translate(); t.translateX=(float) p.getX()-offSetx; t.translateY=(float)
	 * p.getY()-offSety; t.translateZ=(float) p.getZ()-offSetz;
	 * root.addEdgeBitsTo(t,Graph.BRANCH_EDGE, null); Node old_node =t;
	 * 
	 * for(int i = 0; i<points.size()-1;i++) { old_node =
	 * points.get(i+1).addToGraph(points.get(i), old_node, allRoots); } return
	 * old_node; }
	 */
	public String getTag() {
		if (tag == null) {
			return "xxx";
		}
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public void setIsLeaf(boolean isLeaf) {
		points.get(isLeafId).setIsLeaf(isLeaf);
		isLeafId++;
	}

}

class Point {
	ArrayList<Integer> childs;
	float x, y, z;
	Vector3d start;
	float d = (float) 0.1;
	boolean isLeaf = false;

	public Point(float x, float y, float z) {
		this.start = new Vector3d(x, y, z);
		childs = new ArrayList<Integer>();

	}

	void setDiameter(float d) {
		this.d = d;
	}

	float getDiameter() {
		return d;
	}

	public boolean getIsLeaf() {
		return isLeaf;
	}

	public void setIsLeaf(boolean isLeaf) {
		this.isLeaf = isLeaf;
	}

	public double getX() {
		return start.x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public double getY() {
		return start.y;
	}

	public double getZ() {
		return start.z;
	}

	public float getD() {
		return d;
	}

	public void addChild(int kiddoId) {
		childs.add(kiddoId);
	}

//	public Node addToGraph(Point old, Node n, ArrayList<Branch> allRoots) {
//		Null neu;
//		Matrix3d m2 = new Matrix3d();
//
//		if (isLeaf) {
//			neu = new Parallelogram((float) getDistance(old, this), this.getD());
//			m2.rotZ(1.5708); // 90 degree
//
//		} else {
//			neu = new F((float) getDistance(old, this), this.getD());
//			m2.rotZ(0);
//
//		}
//
//		neu.setTransform(new TMatrix4d());
//		Matrix3d m = new Matrix3d();
//		Math2.getOrthogonalBasis(new Vector3d(start.x - old.start.x, start.y - old.start.y, start.z - old.start.z), m,
//				true);
//		m.mul(m2);
//		((TMatrix4d) neu.getTransform()).setRotationScale(m);
//		FastrakN xy = new FastrakN();
//		xy.setTransform(new TMatrix4d());
//		((TMatrix4d) xy.getTransform()).invert((Matrix4d) neu.getTransform());
//
//		n.addEdgeBitsTo(neu, Graph.SUCCESSOR_EDGE, null);
//		neu.addEdgeBitsTo(xy, Graph.SUCCESSOR_EDGE, null);
//
//		if (this.childs.size() > 0) {
//			for (int c : childs) {
//				Node cn = new Node();
//				xy.addEdgeBitsTo(cn, Graph.BRANCH_EDGE, null);
////				allRoots.get(c).createGraph(cn, allRoots, (float) getX(), (float) getY(), (float) getZ(),null,null);
//			}
//
//		}
//		return xy;
//	}

	public double getDistance(Point p, Point pn) {
		double len = Math.sqrt(Math.pow(p.getX() - pn.getX(), 2) + Math.pow(p.getY() - pn.getY(), 2)
				+ Math.pow(p.getZ() - pn.getZ(), 2));
		return len;
	}
}