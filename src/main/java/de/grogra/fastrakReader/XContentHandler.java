package de.grogra.fastrakReader;

import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

import org.xml.sax.helpers.DefaultHandler;

public class XContentHandler extends DefaultHandler {
	private ArrayList<Branch> allBranches = new ArrayList<Branch>();
	private String currentValue;
	private final StringBuilder characters = new StringBuilder(64);
	String tag = "";
	int rootid = -1;
	int shootId = -1;
	int currentId = -1;
	int order = -1;
	boolean isLeaf=false;
	String diameterKey;
	String leafKey;

	float x = 1000000000, y = 1000000000, z = 1000000000, diameter = 1000000000;

	Branch last;

	public XContentHandler(String diameterKey, String leafKey) {
		this.diameterKey = diameterKey;
		this.leafKey = leafKey;
	}

	// getter
	public ArrayList<Branch> getAllBranches() {
		return allBranches;
	}

//implementing ContentHandler
	public void setDocumentLocator(Locator locator) {
		// TODO Auto-generated method stub

	}

	public void startDocument() throws SAXException {
		// TODO Auto-generated method stub

	}

	public void endDocument() throws SAXException {
		// TODO Auto-generated method stub

	}

	public void startPrefixMapping(String prefix, String uri) throws SAXException {
		// TODO Auto-generated method stub

	}

	public void endPrefixMapping(String prefix) throws SAXException {
		// TODO Auto-generated method stub

	}

	public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
	}

	public void endElement(String uri, String localName, String qName) throws SAXException {
		currentValue = characters.toString().trim();
		// .... deal with content
		// reset characters
		characters.setLength(0);
		if (localName.equals("Table1")) {
			if (diameter != 1000000000) {
				allBranches.get(currentId).addPoint(x, y, z);
				allBranches.get(currentId).addDiameter(diameter);
				allBranches.get(currentId).setIsLeaf(isLeaf);
			}
		}

		if (localName.equals("Topo")) {
			/*
			 * if(tag.contains("r")) { System.out.println(tag);
			 * allRoots.get(currentId).setTag(tag); }
			 */
			if (currentValue.contains("A1") && (diameter != 1000000000 || (currentId == -1))) {
				Branch r = new Branch(0);
				if (currentId == -1) {
					System.out.println("root" + x);
					r = new Branch(-1);
					r.setTag("");
				}
				/*
				 * if(currentId!=-1){ allRoots.get(currentId); }
				 */
				// last = r;
				allBranches.add(r);
				currentId = allBranches.size() - 1;
				allBranches.get(currentId).setRootId(currentId);
			}
		}
		if (localName.equals("X")) {
			x = Float.parseFloat(currentValue.replace(",", "."));
		}
		if (localName.equals("Y")) {
			y = Float.parseFloat(currentValue.replace(",", "."));
		}
		if (localName.equals("Z")) {
			z = Float.parseFloat(currentValue.replace(",", "."));
		}
		if (localName.equals(diameterKey)) {
			if (!currentValue.equals("")) {
				try {
					diameter = Float.parseFloat(currentValue.replace(",", ".").replace("..", "."));
				} catch (Exception e) {
					System.err.println(currentValue);
				}
			}
		}
		if (localName.equals("code1")) {
			isLeaf = (currentValue.equals(leafKey));
		}
		
	}

	public void characters(char[] ch, int start, int length) throws SAXException {
//		 currentValue = new String(ch, start, length);
		characters.append(new String(ch, start, length));

	}

	public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
		// TODO Auto-generated method stub

	}

	public void processingInstruction(String target, String data) throws SAXException {
		// TODO Auto-generated method stub

	}

	public void skippedEntity(String name) throws SAXException {
		// TODO Auto-generated method stub

	}
}
